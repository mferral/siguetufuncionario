cargarDenuncias();
function cargarDenuncias(id){
	$("#lista_denuncias").load("/lista_denuncias/"+$("#idtarea").val());	
}


function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                	cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                	break;
                }
            }
        }
        return cookieValue;
    }

    $(document).ready(function() {

    	var csrftoken = getCookie('csrftoken');

    	function csrfSafeMethod(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        $.ajaxSetup({
            crossDomain: false, // obviates need for sameOrigin test
            beforeSend: function(xhr, settings) {
            	if (!csrfSafeMethod(settings.type)) {
            		xhr.setRequestHeader("X-CSRFToken", csrftoken);
            	}
            }
        });

        // This function must be customized


        var onCreate = function(){
        	var url ='/denuncia_create/';           	
        	$.ajax({
        		type: 'post',
        		url: url,
        		data: $(this).serialize(),
        		success: function(datos) {
        			if (datos.result == "ok"){
        				cargarDenuncias();		
        				$("#id_denuncia").val("");
        			} else {
        				//alert("NO Funciona");						        			
        			}
        		}
        	});	        
            return false;
        }

        $("#frmDenuncia").submit(onCreate);
    });