
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                	cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                	break;
                }
            }
        }
        return cookieValue;
    }

    $(document).ready(function() {

    	var csrftoken = getCookie('csrftoken');

    	function csrfSafeMethod(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        $.ajaxSetup({
            crossDomain: false, // obviates need for sameOrigin test
            beforeSend: function(xhr, settings) {
            	if (!csrfSafeMethod(settings.type)) {
            		xhr.setRequestHeader("X-CSRFToken", csrftoken);
            	}
            }
        });

        // This function must be customized
        var onLike = function(){
        	control=$(this)
        	cuantomegusta=$("#cuantomegusta"+control.attr('idfuncionario'));
        	cuantonomegusta=$("#cuantonomegusta"+control.attr('idfuncionario'));
        	idfuncionario=$(this).attr('idfuncionario');
        	megusta=$(this).attr('like');
        	controlHermano="";
        	if (megusta==1){
        		console.log('entro')
        		controlHermano=$("#nomegusta"+idfuncionario)	
        	}else{
        		controlHermano=$("#megusta"+idfuncionario)	
        	}
        	var url ='/guarda_like/';		        			        
		        
				$.post(url,{megusta:megusta,idfuncionario:idfuncionario}, function(data) {
					switch(data) {
					    case '1':
					        control.css('color','#53a7ea');
					        break;
					    case '2':
					        control.css('color','#DE5858');
					        break;
					    case '3':
					        control.css('color','#9C9C9C');
					        break;					      
					}
					controlHermano.css('color','#9C9C9C');

					$.post('cuenta_like/',{idfuncionario:idfuncionario},function(cuantos){
						cuanto=cuantos.split(",");
						cuantomegusta.text(cuanto[0]);
						cuantonomegusta.text(cuanto[1]);
					})
				});	          			            		        		        
            return false;
        }

        $(".megusta").click(onLike);

    });