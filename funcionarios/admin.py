from django.contrib import admin

# Register your models here.
from .models import Funcionario,Dependencia,Estado,Tarea

admin.site.register(Funcionario)
admin.site.register(Dependencia)
admin.site.register(Estado)
admin.site.register(Tarea)