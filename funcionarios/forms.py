from django import forms
from .models import Denuncia
from django.forms import Textarea

class DenunciaForm(forms.ModelForm):
    class Meta:
        model = Denuncia
        exclude = ()
        widgets={            
            "denuncia":forms.Textarea(attrs={'required':'True','rows':'1','class':'mdl-textfield__input'}),            
        }
