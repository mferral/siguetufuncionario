# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import cuser.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Dependencia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=300)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Funcionario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=300)),
                ('puesto', models.CharField(max_length=300)),
                ('domicilio', models.CharField(max_length=500)),
                ('telefonos', models.CharField(max_length=150)),
                ('correo', models.CharField(max_length=150)),
                ('fotografia', models.ImageField(upload_to=b'fotografias')),
                ('dependencia', models.ForeignKey(to='funcionarios.Dependencia')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FuncionarioMeGusta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('megusta', models.BooleanField(default=False)),
                ('funcionario', models.ForeignKey(to='funcionarios.Funcionario')),
                ('usuario', cuser.fields.CurrentUserField(related_name='usuario_fb', editable=False, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
