# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('funcionarios', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('estado', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tareas',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('meta', models.TextField()),
                ('resultado', models.TextField(null=True, blank=True)),
                ('estado', models.ForeignKey(to='funcionarios.Estado')),
                ('funcionario', models.ForeignKey(to='funcionarios.Funcionario')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
