# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('funcionarios', '0002_estado_tareas'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Tareas',
            new_name='Tarea',
        ),
    ]
