# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('funcionarios', '0003_auto_20150924_1552'),
    ]

    operations = [
        migrations.AddField(
            model_name='tarea',
            name='fecha',
            field=models.DateTimeField(default=None, auto_now_add=True),
            preserve_default=False,
        ),
    ]
