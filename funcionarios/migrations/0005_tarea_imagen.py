# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('funcionarios', '0004_tarea_fecha'),
    ]

    operations = [
        migrations.AddField(
            model_name='tarea',
            name='imagen',
            field=models.ImageField(null=True, upload_to=b'resultados', blank=True),
            preserve_default=True,
        ),
    ]
