# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import cuser.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('funcionarios', '0005_tarea_imagen'),
    ]

    operations = [
        migrations.CreateModel(
            name='TareaMeGusta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('megusta', models.BooleanField(default=False)),
                ('tarea', models.ForeignKey(to='funcionarios.Tarea')),
                ('usuario', cuser.fields.CurrentUserField(editable=False, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
