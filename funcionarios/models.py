from django.db import models
from cuser.fields import CurrentUserField

class Dependencia(models.Model):
	nombre=models.CharField(max_length=300)

	def __unicode__(self):
		return self.nombre

# Create your models here.
class Funcionario(models.Model):
	nombre=models.CharField(max_length=300)
	puesto=models.CharField(max_length=300)
	domicilio=models.CharField(max_length=500)
	telefonos=models.CharField(max_length=150)
	correo=models.CharField(max_length=150)	
	fotografia = models.ImageField(upload_to='fotografias')
	dependencia = models.ForeignKey(Dependencia)
	def __unicode__(self):
		return self.nombre 

class FuncionarioMeGusta(models.Model):
	funcionario = models.ForeignKey(Funcionario)	
	usuario = CurrentUserField(related_name="usuario_fb")
	megusta=models.BooleanField(default=False)		
	def __unicode__(self):
		return str(self.usuario.id)

class Estado(models.Model):
	estado=models.CharField(max_length=50)
	def __unicode__(self):
		return self.estado 

class Tarea(models.Model):	
	meta=models.TextField()
	resultado=models.TextField(null=True, blank=True)
	imagen = models.ImageField(upload_to="resultados", blank=True, null=True)
	fecha = models.DateTimeField(auto_now_add=True, blank=True)
	funcionario = models.ForeignKey(Funcionario)
	estado = models.ForeignKey(Estado)
	def __unicode__(self):
		return self.meta	

class TareaMeGusta(models.Model):
	tarea = models.ForeignKey(Tarea)	
	usuario = CurrentUserField()
	megusta=models.BooleanField(default=False)		
	def __unicode__(self):
		return str(self.usuario.id)

class Denuncia(models.Model):
	tarea = models.ForeignKey(Tarea)	
	usuario = CurrentUserField()
	denuncia=models.TextField()
	fecha = models.DateTimeField(auto_now_add=True, blank=True)

	class Meta:
		ordering = ["-fecha"]

	def __unicode__(self):
		return self.denuncia	