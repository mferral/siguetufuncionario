from django.shortcuts import render
from django.views.generic import ListView
from .models import Funcionario
from .models import FuncionarioMeGusta, Tarea, Denuncia
from .models import TareaMeGusta
from django.db.models import Q
from cuser.middleware import CuserMiddleware
from django.http import HttpResponse
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from .forms import DenunciaForm
from django.http import JsonResponse

# Create your views here.
class FuncionariosListView(ListView):
	model = Funcionario
	template_name = 'index.html'
	context_object_name = "funcionarios" 

	def get_context_data(self, **kwargs):
		context = super(FuncionariosListView, self).get_context_data(**kwargs)
		return context

	def get_queryset(self):
		user = CuserMiddleware.get_user()
		f3=super(FuncionariosListView,self).get_queryset().order_by('nombre')
		query=[]	
		user=CuserMiddleware.get_user()		
		f3=f3.order_by('nombre')		
		for funcionario in f3:
			item={}	
			item['funcionario']=funcionario				
			if not user.is_anonymous():
				fmg=FuncionarioMeGusta.objects.filter(Q(usuario=user) & Q(funcionario__id=funcionario.id))
				if fmg.exists():
					item['like']=fmg[0].megusta
			megusta=FuncionarioMeGusta.objects.filter(Q(megusta=True) & Q(funcionario__id=funcionario.id)).count()
			nomegusta=FuncionarioMeGusta.objects.filter(Q(megusta=False) & Q(funcionario__id=funcionario.id)).count()
			item['megusta']=megusta
			item['nomegusta']=nomegusta	
			print funcionario	
			query.append(item)
		queryset= query
		return queryset

class FuncionarioDetailView(DetailView):
    model = Funcionario
    context_object_name = 'funcionario'
    template_name = 'ficha.html'

    def get_context_data(self, **kwargs):
		context = super(FuncionarioDetailView, self).get_context_data(**kwargs)
		user=CuserMiddleware.get_user()
		idfuncionario=self.kwargs['pk']
		tareas=Tarea.objects.filter(funcionario__id=idfuncionario)
		tasks=[]
		for tarea in tareas:
			item={}
			item['tarea']=tarea
			if not user.is_anonymous():
				tmg=TareaMeGusta.objects.filter(Q(usuario=user) & Q(tarea__id=tarea.id))
				if tmg.exists():
					item['like']=tmg[0].megusta
			megusta=TareaMeGusta.objects.filter(Q(megusta=True) & Q(tarea__id=tarea.id)).count()
			nomegusta=TareaMeGusta.objects.filter(Q(megusta=False) & Q(tarea__id=tarea.id)).count()
			denuncias=Denuncia.objects.filter(tarea__id=tarea.id).count()
			item['megusta']=megusta
			item['nomegusta']=nomegusta
			item['denuncias']=denuncias
			tasks.append(item)
		context['tareas']=tasks
		return context

class TareaDetailView(DetailView):
    model = Tarea
    context_object_name = 'tarea'    
    template_name = 'denuncias.html'

    def get_context_data(self, **kwargs):
		context = super(TareaDetailView, self).get_context_data(**kwargs)
		context['form'] = DenunciaForm
		user=CuserMiddleware.get_user()
		idtarea=self.kwargs['pk']
		if not user.is_anonymous():
			tmg=TareaMeGusta.objects.filter(Q(usuario=user) & Q(tarea__id=idtarea))
			if tmg.exists():
				context['like']=tmg[0].megusta
		megusta=TareaMeGusta.objects.filter(Q(megusta=True) & Q(tarea__id=idtarea)).count()
		nomegusta=TareaMeGusta.objects.filter(Q(megusta=False) & Q(tarea__id=idtarea)).count()
		context['megusta']=megusta
		context['nomegusta']=nomegusta
		return context		


def guarda_like(request):
	color=0 #1:azul,2:rojo,3:gris,
	if request.POST:		
		megusta=int(request.POST['megusta'])
		user=CuserMiddleware.get_user()
		idfuncionario=request.POST['idfuncionario']		
		funcionario=Funcionario.objects.get(pk=idfuncionario)		
		funcionariomegusta=FuncionarioMeGusta.objects.filter(funcionario=funcionario,usuario=user)
		if funcionariomegusta.exists():
			if funcionariomegusta[0].megusta==megusta:
				funcionariomegusta.delete()
				color=3
			else:						
				funcionariomegusta.update(megusta=megusta)
				if megusta==True:
					color=1
				else:
					color=2				
		else:
			if megusta==True:
				color=1
			else:
				color=2
			FuncionarioMeGusta.objects.create(megusta=megusta,funcionario=funcionario)		
	return HttpResponse(color)

def cuenta_like(request):
	megusta='0'
	nomegusta='0'
	if request.POST:
		idfuncionario=request.POST['idfuncionario']
		funcionario=Funcionario.objects.get(pk=idfuncionario)		
		megusta=FuncionarioMeGusta.objects.filter(Q(funcionario=funcionario) & Q(megusta=True)).count()
		nomegusta=FuncionarioMeGusta.objects.filter(Q(funcionario=funcionario) & Q(megusta=False)).count()		
	cadena=str(megusta)+','+str(nomegusta)
	return HttpResponse(cadena)

def guarda_like_tarea(request):
	color=0 #1:azul,2:rojo,3:gris,
	if request.POST:		
		megusta=int(request.POST['megusta'])
		user=CuserMiddleware.get_user()
		idtarea=request.POST['idtarea']		
		tarea=Tarea.objects.get(pk=idtarea)		
		tareamegusta=TareaMeGusta.objects.filter(tarea=tarea,usuario=user)
		if tareamegusta.exists():
			if tareamegusta[0].megusta==megusta:
				tareamegusta.delete()
				color=3
			else:						
				tareamegusta.update(megusta=megusta)
				if megusta==True:
					color=1
				else:
					color=2				
		else:
			if megusta==True:
				color=1
			else:
				color=2
			TareaMeGusta.objects.create(megusta=megusta,tarea=tarea)		
	return HttpResponse(color)

def cuenta_like_tarea(request):
	megusta='0'
	nomegusta='0'
	if request.POST:
		idtarea=request.POST['idtarea']
		tarea=Tarea.objects.get(pk=idtarea)		
		megusta=TareaMeGusta.objects.filter(Q(tarea=tarea) & Q(megusta=True)).count()
		nomegusta=TareaMeGusta.objects.filter(Q(tarea=tarea) & Q(megusta=False)).count()		
	cadena=str(megusta)+','+str(nomegusta)
	return HttpResponse(cadena)


class DenunciasListView(ListView):
	model = Denuncia
	template_name = 'lista_denuncias.html'
	context_object_name = "denuncias" 

	def get_context_data(self, **kwargs):
		context = super(DenunciasListView, self).get_context_data(**kwargs)
		return context

	def get_queryset(self):
		print self.kwargs['pk']
		queryset=self.model.objects.filter(tarea__id=self.kwargs['pk'])
		return queryset		


class DenunciaCreate(CreateView):
	model = Denuncia
	form_class = DenunciaForm
	template_name = 'lista_denuncias.html'

	def form_valid(self, form, **kwargs):
   		context = self.get_context_data(**kwargs)
   		context['form'] = form
   		form.save()
   		return self.render_to_response(context)	

	def dispatch(self, *args, **kwargs):
		resp = super(DenunciaCreate, self).dispatch(*args, **kwargs)
		if self.request.is_ajax():
			response_data = {"result": "ok"}
			return JsonResponse(response_data)
		else:
			return resp	