from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from funcionarios.views import FuncionariosListView,DenunciasListView, FuncionarioDetailView, TareaDetailView,DenunciaCreate
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'siguetufuncionario.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social')),
    #url(r'^$', TemplateView.as_view(template_name="index.html")),
    url(r'^$', FuncionariosListView.as_view()),  
    url(r'^guarda_like/', 'funcionarios.views.guarda_like'),
    url(r'^cuenta_like/', 'funcionarios.views.cuenta_like'),
    url(r'^guarda_like_tarea/', 'funcionarios.views.guarda_like_tarea'),
    url(r'^cuenta_like_tarea/', 'funcionarios.views.cuenta_like_tarea'),    
    url(r'^logout/$', 'base.views.logout'),  
    #url(r'^ficha/', TemplateView.as_view(template_name="ficha.html")),
    url(r'^ficha/(?P<pk>[\d]+)', FuncionarioDetailView.as_view()),
    url(r'^denuncias/(?P<pk>[\d]+)', TareaDetailView.as_view()),
    url(r'^lista_denuncias/(?P<pk>[\d]+)', DenunciasListView.as_view()),
    url(r'^denuncia_create/', DenunciaCreate.as_view()),
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)